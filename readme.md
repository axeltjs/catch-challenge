
## About This Apps

First of all we have to install all the needed libraries `composer install`. 
after that we need to setup .env file, just like another laravel application. and then run `php artisan migrate`

to run this apps just simply run `php artisan download`

## OR

if you're already installed this apps, please run `php artisan migrate:fresh` to clear previous data.
