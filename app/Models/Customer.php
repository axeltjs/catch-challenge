<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'order_id',
        'customer_id',
        'order_datetime',
        'total_order_value',
        'distinct_unit_count',
        'discount',
        'average_unit_price',
        'customer_state',
    ];

    public function customerDiscount()
    {
        return $this->hasMany(CustomerDiscount::class, 'customer_id', 'customer_id');
    }
}
