<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDiscount extends Model
{
    protected $fillable = [
        'customer_id',
        'type',
        'value',
        'priority',
    ];
}
