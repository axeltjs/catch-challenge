<?php

namespace Command;

use App\Exports\CustomerExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Models\Customer;
use App\Models\CustomerDiscount;

trait TraitCommandHelper
{
    /**
     * Export file to CSv.
     *
     * @return info
     */
    public function exportToCsv()
    {
        Excel::store(new CustomerExport(), 'out.csv');

        return $this->info('Success to download the output file of .csv');
    }

    /**
     * Export file to Xls.
     *
     * @return info
     */
    public function exportToXls()
    {
        Excel::store(new CustomerExport(), 'out.xlsx');

        return $this->info('Success to download the output file of .xlsx');
    }

    /**
     * Export file to json.
     *
     * @return info
     */
    public function exportToJson()
    {
        $data = Customer::all()->toJson();

        Storage::put('out.json', $data);

        return $this->info('Success to download the output file of .json');
    }

    /**
     * Insert to database.
     *
     * @return collection
     */
    public function insertToDatabase($data)
    {
        return Customer::insert($data);
    }

    public function insertDiscountToDatabase($data)
    {
        return CustomerDiscount::insert($data);
    }

    public function calculateDiscount($discount, $calculation, $discountValue)
    {
        $priceFinal = 0;
        if ($discount->type === 'DOLLAR') {
            $discountValue = $discount->value;
        } else {
            $discountValue = $discount->value / 100 * $calculation['price'];
        }

        $priceFinal = $calculation['price'] - $discountValue;
        $data = ['priceFinal' => $priceFinal, 'discount' => $discountValue];

        return $data;
    }

    /**
     * route user option.
     *
     * @return object
     */
    public function execUserOption($type)
    {
        if ($type == '.csv') {
            return $this->exportToCsv();
        } elseif ($type == '.xlsx') {
            return $this->exportToXls();
        } else {
            return $this->exportToJson();
        }
    }

    /**
     * Processing data from json.
     *
     * @return array
     */
    public function calculateData($customerItem)
    {
        $price = 0; //set default value for price
        $avg = 0; //set default value for avg price
        $unitCount = 0; //set default value for counting quantity of items
        $productArray = []; //set array for checking how many items is unique

        foreach ($customerItem as $item) {
            $price += $item->quantity * $item->unit_price;
            $avg += $item->unit_price;
            $unitCount += $item->quantity;
            array_push($productArray, $item->product->title);
        }

        $arrayUniqueItem = array_count_values($productArray);
        $countDistinctItem = 0;

        foreach ($arrayUniqueItem as $item) {
            if ($item == 1) {
                ++$countDistinctItem;
            }
        }

        $finalAvg = $avg / count($customerItem);
        $price = number_format((float) $price, 2, '.', '');
        $finalAvg = number_format((float) $finalAvg, 2, '.', '');

        $finalData = [
            'finalAvg' => $finalAvg,
            'price' => $price,
            'countDistinctItem' => $countDistinctItem,
        ];

        return $finalData;
    }

    /**
     * Get data from S3.
     *
     * @return jsonl
     */
    private function getData()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'cache-control: no-cache',
                'content-type : application/json',
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $data = [
            'response' => $response,
            'error' => $err,
        ];

        return $data;
    }
}
