<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmailCommand extends Command
{
    protected $signature = 'email:sendto {email} {ext}';

    protected $description = 'Sending emails to the users.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data = array(
            'name' => 'xxx',
       );
        $getEmail = $this->argument('email');
        $ext = $this->argument('ext');
        Mail::send('emails.file', $data, function ($message) use ($getEmail, $ext) {
            $message->from('xxx@gmail.com');
            $message->attach(storage_path('app/out'.$ext), [
                'as' => 'Customer data',
                'mime' => 'application/'.$ext,
              ]);
            $message->to($getEmail)->subject("Here's your file!");
        });
        $this->info('The emails are send successfully!');
    }
}
