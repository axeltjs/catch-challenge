<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Illuminate\Console\Command;
use Rs\JsonLines\JsonLines;
use Illuminate\Support\Facades\Artisan;
use Command\TraitCommandHelper;

class DownloadCommand extends Command
{
    use TraitCommandHelper;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloading some data file such as .json, .csv, and .xlsx';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->choice('What kind of file types do you want?', ['.csv', '.xlsx', '.json'], 0);

        $checkData = Customer::count();
        if ($checkData == 0) {
            $data = $this->getData();
            if ($data['error']) {
                $this->info('cURL Error #:'.$data['error']);
            } else {
                $json = (new JsonLines())->deline($data['response']);
                $jsonDecode = json_decode($json);
                $newData = [];
                $newDataDiscount = [];
                foreach ($jsonDecode as $cust) {
                    $calculation = $this->calculateData($cust->items);
                    $discountValue = 0;
                    if (!empty($cust->discounts)) {
                        foreach ($cust->discounts as $discount) {
                            $newDataDiscount[] = [
                                'customer_id' => $cust->customer->customer_id,
                                'type' => $discount->type,
                                'value' => $discount->value,
                                'priority' => $discount->priority,
                            ];
                            $selectionResult = $this->calculateDiscount($discount, $calculation, $discountValue);
                            $discountValue = $selectionResult['discount'];
                        }
                    }
                    $newData[] = [
                        'customer_id' => $cust->customer->customer_id,
                        'order_id' => $cust->order_id,
                        'order_datetime' => $cust->order_date,
                        'discount' => $discountValue,
                        'total_order_value' => $calculation['price'],
                        'total_order_value_with_discount' => $calculation['price'] - $discountValue,
                        'distinct_unit_count' => $calculation['countDistinctItem'],
                        'average_unit_price' => $calculation['finalAvg'],
                        'customer_state' => $cust->customer->shipping_address->state,
                    ];
                }
                $this->insertToDatabase($newData);
                $this->insertDiscountToDatabase($newDataDiscount);
                $this->execUserOption($type);
            }
        } else {
            $this->execUserOption($type);
        }

        return $this->sendEmail($type);
    }

    public function sendEmail($type)
    {
        $email = $this->ask('What is your email?');
        if (isset($email)) {
            Artisan::call('email:sendto', [
                'email' => $email,
                'ext' => $type,
            ]);
        }
        $this->info('done!');
    }
}
