<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Customer;

class CustomerExport implements FromCollection, WithHeadings
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Customer::select([
            'order_id',
            'customer_id',
            'order_datetime',
            'total_order_value',
            'discount',
            'total_order_value_with_discount',
            'distinct_unit_count',
            'average_unit_price',
            'customer_state',
        ])->get();
    }

    public function headings(): array
    {
        return [
            'order_id',
            'customer_id',
            'order_datetime',
            'total_order_value',
            'discount',
            'total_order_value_with_discount',
            'distinct_unit_count',
            'average_unit_price',
            'customer_state',
        ];
    }
}
